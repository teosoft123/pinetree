package com.tsvinev.anagram;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AnagramTest {

    @DataProvider
    public Object[][] anagramData() {
        return new Object[][] {
                new Object[] {"123", "312", true},
                new Object[] {"123", "313", false},
                new Object[] {"123", "31", false},
                new Object[] {"123454", "445321", true},
                new Object[] {"rail safety", "fairy tales", true},
                new Object[] {"William Shakespeare", "I am a weakish speller", true},
                new Object[] {"Maße", "ßemA", true},
                new Object[] {"Madam Curie", "Radium came", true},
                new Object[] {"Quid est veritas", "Est vir qui adest", true},
                new Object[] {"Vladimir Nabokov", "Vivian Darkbloom", true},
        };
    }

    @Test(dataProvider = "anagramData")
    public void mustDetectAnagrams(String s1, String s2, boolean isAnagram) {
        Assert.assertEquals(Anagram.isAnagram(s1, s2), isAnagram);
    }
}
