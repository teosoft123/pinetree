package com.tsvinev.shortestpath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.IntStream;

public class ShortestPath {

    public static class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        static Point left(Point p) {
            return new Point(p.x-1, p.y);
        }
        static Point right(Point p) {
            return new Point(p.x+1, p.y);
        }
        static Point up(Point p) {
            return new Point(p.x, p.y-1);
        }
        static Point down(Point p) {
            return new Point(p.x, p.y+1);
        }

        public boolean sameAs(Point other) { // of course equals and hashCode but this one will do
            if(null == other) {
                return false;
            }
            return x == other.x && y == other.y;
        }
    }

    private static boolean canVisit(Point p, int[][] maze, boolean[][] visited) {
        return maze[p.y][p.x] != 1
                && !visited[p.y][p.x]
                && 0 <= p.y && p.y < maze.length && 0 <= p.x && p.x < maze[0].length;
    }

    public static List<Point> shortestPath(int[][] input, Point start, Point end) {
        if (null == input || input.length == 0) {
            return Collections.emptyList();
        }
        boolean visited[][] = new boolean[input.length][input[0].length];

        List<Point> path = new ArrayList<>();
        Queue<Point> q = new LinkedList<>();
        q.add(start);
        path.add(start);
        while(!q.isEmpty()) {
            Point p = q.remove();
            path.add(p);
            visited[p.y][p.x] = true;
            if(p.sameAs(end)) {
                return path;
            }
            if(canVisit(p.left(p), input, visited)) {
                q.add(p.left(p));
            }
            if(canVisit(p.right(p), input, visited)) {
                q.add(p.right(p));
            }
            if(canVisit(p.up(p), input, visited)) {
                q.add(p.up(p));
            }
            if(canVisit(p.down(p), input, visited)) {
                q.add(p.down(p));
            }
        }
        return Collections.emptyList();
    }

    public static void main(String[] args) {
        System.out.println(shortestPath(
                new int[][] {
                    {0,0,0,0},
                    {0,0,1,0},
                    {0,0,2,0},
                },
                new Point(0, 0), new Point(2, 2)));
    }

}
