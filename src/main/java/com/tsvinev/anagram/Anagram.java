package com.tsvinev.anagram;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class Anagram {

    public static Stream<Character> normalize(String s) {
        return s.chars().mapToObj(c -> (char) c).map(c -> Character.toLowerCase(c))
                .filter(c -> !Character.isSpaceChar(c));
    }

    public static boolean isAnagram(String s1, String s2) {
        Map<Character, AtomicInteger> map = new HashMap<>();

        normalize(s1).forEach(c -> {
            map.putIfAbsent(c, new AtomicInteger(0));
            map.get(c).incrementAndGet();
        });
        normalize(s2).forEach(c -> {
            if (map.containsKey(c)) {
                if (0 == map.get(c).decrementAndGet()) {
                    map.remove(c);
                }
            } else {
                return;
            }
        });
        return map.isEmpty();
    }

    public static void main(String... args) {
        System.out.println(isAnagram("123", "312"));
        System.out.println(isAnagram("123", "313"));
        System.out.println(isAnagram("123", "31"));
        System.out.println(isAnagram("123454", "445321"));
        System.out.println(isAnagram("rail safety", "fairy tales"));
        System.out.println(isAnagram("William Shakespeare", "I am a weakish speller"));
        System.out.println(isAnagram("Maße", "ßemA"));
        System.out.println(isAnagram("Madam Curie", "Radium came"));
        System.out.println(isAnagram("Quid est veritas", "Est vir qui adest"));
        System.out.println(isAnagram("Vladimir Nabokov", "Vivian Darkbloom"));
    }
}
