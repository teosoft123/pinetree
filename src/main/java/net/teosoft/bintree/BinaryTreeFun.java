package net.teosoft.bintree;

import static net.teosoft.bintree.Node.mkn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntConsumer;

public class BinaryTreeFun {

    /**
     * Simple, naive, in-order binary tree printing
     * @param n tree root
     */
    public static void printTree(Node n) {
        if (null != n) {
            if (null == n.left && null == n.right) {
                System.out.println(n.data);
            } else {
                printTree(n.left);
                System.out.println(n.data);
                printTree(n.right);
            }
        }
    }

    /**
     * Binary tree walker with pluggable output sink
     * @param n tree root
     * @param s Accumulator
     * @return
     */
    public static Sink walkTree(Node n, Sink s) {
        if (null != n) {
            if (null == n.left && null == n.right) {
                s.add(n.data);
            } else {
                walkTree(n.left, s);
                s.add(n.data);
                walkTree(n.right, s);
            }
        }
        return s;
    }

    /** Java 8 style binary tree walker - no need for interface!
     * @param n tree root
     * @param s Lambda consuming int
     */
    public static void walkTree(Node n, IntConsumer s) {
        if (null != n) {
            if (null == n.left && null == n.right) {
                s.accept(n.data);
            } else {
                walkTree(n.left, s);
                s.accept(n.data);
                walkTree(n.right, s);
            }
        }
    }

    public static Node searhTree(Node n, int key, IntConsumer searchPath) {
        searchPath.accept(null != n ? n.data : null);
        if(null == n || n.data == key) {
            return n;
        }
        if(n.data > key) {
            return searhTree(n.left, key, searchPath);
        }
        return searhTree(n.right, key, searchPath);
    }

    public static Node searhTreeIteratively(Node n, int key, IntConsumer searchPath) {
        Node currentNode = n;
        while(null != currentNode) {
            searchPath.accept(null != currentNode ? currentNode.data : null);
            if(currentNode.data == key) {
                return currentNode;
            }
            if(currentNode.data > key) {
                currentNode = currentNode.left;
            }
            else {
                currentNode = currentNode.right;
            }
        }
        return currentNode;
    }

    public static void walkTreeIteratively(Node n, IntConsumer acceptor) {
        Stack<Node> stack = new Stack<>();
        Node currentNode = n;
        while (null != currentNode) {
            stack.push(currentNode);
            currentNode = currentNode.left;
        }
        while (!stack.isEmpty()) {
            currentNode = stack.pop();
            acceptor.accept(currentNode.data);
            if (null != currentNode.right) {
                currentNode = currentNode.right;
                while (null != currentNode) {
                    stack.push(currentNode);
                    currentNode = currentNode.left;
                }
            }
        }

    }

    public static void walkTreeBFS(Node n, IntConsumer c) {
        if(null != n) {
            Queue<Node> q = new LinkedList<>();
            q.add(n);

            while(!q.isEmpty()) {
                Node current = q.remove();
                c.accept(current.data);
                if(null != current.left) {
                    q.add(current.left);
                }
                if(null != current.right) {
                    q.add(current.right);
                }
            }
        }
    }

    public static Node readBinTree(int ... sortedArray) {
        if(null == sortedArray) {
            return null;
        }
        return readBinTree(0, sortedArray.length - 1, sortedArray);
    }

    private static Node readBinTree(int low, int high, int[] sortedArray) {
        if (low > high) {
            return null;
        }
        int mid = (low + high) / 2;
        Node n = mkn(sortedArray[mid]);
        n.left = readBinTree(low, mid - 1, sortedArray);
        n.right = readBinTree(mid + 1, high, sortedArray);
        return n;
    }

    public static void main(String[] args) {
        // make a binary tree
        Node n = mkn(mkn(mkn(mkn(mkn(4), 5), 6), 7, mkn(8)), 9, mkn(mkn(10), 11, mkn(12, mkn(13))));

        // Java 7: too much code somewhere else
        printTree(n);
        System.out.println(Arrays.toString(walkTree(n, new ArrayCollectingSink()).get()));
        walkTree(n, new PrintingSink());

        // Java 8: just print: generator
        walkTree(n, System.out::println);

        // Java 8: print and count: counting generator
        AtomicInteger count = new AtomicInteger(0);
        walkTree(n, s -> { System.out.printf("Element #%d is %d%n", count.incrementAndGet(), s); });

        // Java 8: just accumulator
        List<Integer> list = new ArrayList<>();
        walkTree(n, s -> { list.add(s); });

        list.forEach(System.out::println);

        System.out.println();

        n = mkn(mkn(mkn(mkn(mkn(1), 10), 12), 14, mkn(mkn(mkn(mkn(28), 33), 37), 39, mkn(43, mkn(56)))), 68, mkn(mkn(86), 95));
        walkTree(n, System.out::println);

        List<Integer> searchPath = new ArrayList<>();
        Node s1 = searhTree(n, 12, i -> { searchPath.add(i); });
        System.out.printf("key: %d, node.data: %s, search path: %s%n", 12, s1 != null ? s1.data : "null", searchPath);

        list.retainAll(Collections.EMPTY_LIST);
        walkTree(n, s -> { list.add(s); });
        System.out.println(list);

        Node r = readBinTree(1,2,3);
        walkTree(r, System.out::println);

        walkTreeBFS(n, s -> { System.out.printf("%d, ", s); } );
        System.out.println();
        walkTreeBFS(readBinTree(1,2,3,4,5,6), s -> { System.out.printf("%d, ", s); } );

    }

}
