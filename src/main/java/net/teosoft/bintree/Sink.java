package net.teosoft.bintree;

public interface Sink {

    int add(int item);

    int[] get();

}