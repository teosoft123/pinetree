package net.teosoft.bintree;

public class PrintingSink implements Sink {

    @Override
    public int add(int item) {
        System.out.println(item);
        return 0;
    }

    @Override
    public int[] get() {
        return new int[0];
    }

}
