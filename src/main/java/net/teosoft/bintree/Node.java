package net.teosoft.bintree;

public class Node {
    Node left;
    Node right;
    int data;

    public static Node mkn(Node left, int data, Node right) {
        Node n = new Node();
        n.data = data;
        n.left = left;
        n.right = right;
        return n;
    }

    public static Node mkn(int data) {
        return mkn(null, data, null);
    }

    public static Node mkn(Node left, int data) {
        return mkn(left, data, null);
    }

    public static Node mkn(int data, Node right) {
        return mkn(null, data, right);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(left);
        builder.append(", ");
        builder.append(data);
        builder.append(", ");
        builder.append(right);
        builder.append("]");
        return builder.toString();
    }

}
