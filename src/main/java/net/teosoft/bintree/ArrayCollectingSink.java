package net.teosoft.bintree;

import java.util.Arrays;

public class ArrayCollectingSink implements Sink {
    private int index = 0;
    private int capacity = 5;
    private final int capcityGrowthFactor = 2;
    private int[] data = new int[capacity];

    @Override
    public int add(int item) {
        if (index >= data.length) {
            capacity *= capcityGrowthFactor;
            data = Arrays.copyOf(data, capacity);
        }
        data[index++] = item;
        return index;
    }

    @Override
    public int[] get() {
        return Arrays.copyOf(data, index);
    }
}
