package net.teosoft.iterators;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Stream;

public class SplitIteratorFun {

    public static void main(String[] args) {

        Spliterator<Integer> si = Stream.of(0,1,2,3,4,5,6,7,8,9).spliterator();
        List<Spliterator<Integer>> siList = new ArrayList<>();
        while(true) {
            Spliterator<Integer> si1 = si.trySplit();
            if(null == si1) {
                break;
            }
            else {
                siList.add(si);
                siList.add(si1);
                si = si1;
            }
        }
        siList.forEach(s -> { s.forEachRemaining(System.out::println); System.out.println("--------"); });

    }

}
